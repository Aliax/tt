require 'rest-client'

default_host = 'http://127.0.0.1:3000'

logins = ['Aaran', 'Aaren', 'Aarez', 'Aarman', 'Aaron', 'Aaron-James', 'Aarron', 'Aaryan', 'Aaryn', 'Aayan', 'Aazaan',
          'Abaan', 'Abbas', 'Abdallah', 'Abdalroof', 'Abdihakim', 'Abdirahman', 'Abdisalam', 'Abdul', 'Abdul-Aziz',
          'Abdulbasir', 'Abdulkadir', 'Abdulkarem', 'Abdulkhader', 'Abdullah', 'Abdul-Majeed', 'Abdulmalik',
          'Abdul-Rehman', 'Abdur', 'Abdurraheem', 'Abdur-Rahman', 'Abdur-Rehmaan', 'Abel', 'Abhinav', 'Abhisumant',
          'Abid', 'Abir', 'Abraham', 'Abu', 'Abubakar', 'Ace', 'Adain', 'Adam', 'Adam-James', 'Addison', 'Addisson',
          'Adegbola', 'Adegbolahan', 'Aden', 'Adenn', 'Adie', 'Adil', 'Aditya', 'Adnan', 'Adrian', 'Adrien', 'Aedan',
          'Aedin', 'Aedyn', 'Aeron', 'Afonso', 'Ahmad', 'Ahmed', 'Ahmed-Aziz', 'Ahoua', 'Ahtasham', 'Aiadan', 'Aidan',
          'Aiden', 'Aiden-Jack', 'Aiden-Vee', 'Aidian', 'Aidy', 'Ailin', 'Aiman', 'Ainsley', 'Ainslie', 'Airen',
          'Airidas', 'Airlie', 'AJ', 'Ajay', 'A-Jay', 'Ajayraj', 'Akan', 'Akram', 'Al', 'Ala', 'Alan', 'Alanas',
          'Alasdair', 'Alastair', 'Alber', 'Albert', 'Albie', 'Aldred', 'Alec', 'Aled', 'Aleem', 'Aleksandar',
          'Aleksander', 'Aleksandr', 'Aleksandrs', 'Alekzander', 'Alessandro', 'Alessio', 'Alex', 'Alexander',
          'Alexei', 'Alexx', 'Alexzander', 'Alf', 'Alfee', 'Alfie', 'Alfred', 'Alfy', 'Alhaji', 'Al-Hassan',
          'Ali', 'Aliekber', 'Alieu', 'Alihaider', 'Alisdair', 'Alishan', 'Alistair', 'Alistar', 'Alister', 'Aliyaan',
          'Allan', 'Allan-Laiton', 'Allen', 'Allesandro', 'Allister', 'Ally', 'Alphonse', 'Altyiab', 'Alum',
          'Alvern', 'Alvin', 'Alyas', 'Amaan', 'Aman', 'Amani', 'Ambanimoh', 'Ameer', 'Amgad', 'Ami', 'Amin',
          'Amir', 'Ammaar', 'Ammar', 'Ammer', 'Amolpreet', 'Amos', 'Amrinder', 'Amrit', 'Amro', 'Anay', 'Andrea']

(0..200_000).each { |n| RestClient.post "#{default_host}/posts", { login: logins[rand(logins.length)], ip: "172.16.10.#{rand(0..50)}", title: "XaXa_#{n}", content: 'stop manual typing' } }

(0..10_000).each { |n| RestClient.put "#{default_host}/posts/#{rand(Post.count)}", { rate_value: "#{rand(5)}" } }