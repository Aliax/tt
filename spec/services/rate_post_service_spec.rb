# frozen_string_literal: true

require 'rails_helper'

RSpec.describe RatePostService, type: :service do
  describe '#call' do
    let!(:user) { User.create(login: 'Alf') }
    let!(:post) { user.posts.create(title: 'Alf\'s life', content: 'Frendly, eat ants', ip: '172.16.10.2/24') }
    let!(:rate) { Rate.create(post_id: post.id, value: 4) }
    let(:params) { { format: :json, id: post.id, rate_value: 2 } }

    context 'when get valid parameters' do
      it 'response without errors and create new post' do
        response = RatePostService.new(params).call

        expect(response.error_messages).to be_empty
        expect(response.post_rate).to eq 3
      end
    end

    context 'when get invalid parameters' do
      it 'should respond with error empty id' do
        params[:id] = ''
        response = RatePostService.new(params).call

        expect(response.error_messages).to include(I18n.t(:empty_id))
      end

      it 'should respond with error invalid rate value' do
        params[:rate_value] = nil
        response = RatePostService.new(params).call

        expect(response.error_messages).to include(I18n.t(:invalid_rate_value))
      end

      it 'should respond with few error' do
        params[:rate_value] = 6
        params[:id] = ''
        response = RatePostService.new(params).call

        expect(response.error_messages).to eq([I18n.t(:empty_id), I18n.t(:invalid_rate_value)])
      end
    end
  end
end
