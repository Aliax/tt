# frozen_string_literal: true

require 'rails_helper'

RSpec.describe CreatePostService, type: :service do
  describe '#call' do
    let(:params) { { title: 'Note', content: 'I know you dreams', login: 'Tom', ip: '172.16.10.1/24' } }

    context 'when get valid parameters' do
      context 'when user exists' do
        let!(:user) { User.create(login: 'Tom') }

        it 'response without errors and create new post' do
          expect(User.find_by(login: params[:login])).to be

          response = CreatePostService.new(params).call

          expect(response.error_messages).to be_empty
        end
      end

      context 'when user isnt exists' do
        it 'response without errors and create new post' do
          expect(User.find_by(login: params[:login])).to be_nil

          response = CreatePostService.new(params).call

          expect(response.error_messages).to be_empty
          expect(User.find_by(login: params[:login])).to be
        end
      end
    end

    context 'when get invalid parameters' do
      it 'response with invalid_title error' do
        params[:title] = nil
        response = CreatePostService.new(params).call

        expect(response.error_messages).to include(I18n.t(:invalid_title))
      end

      it 'response with invalid_content error' do
        params[:content] = nil
        response = CreatePostService.new(params).call

        expect(response.error_messages).to include(I18n.t(:invalid_content))
      end

      it 'response with invalid_ip_address error' do
        params[:ip] = nil
        response = CreatePostService.new(params).call

        expect(response.error_messages).to include(I18n.t(:invalid_ip_address))
      end

      it 'response with few errors' do
        params = {}
        response = CreatePostService.new(params).call

        expect(response.error_messages).to eq([I18n.t(:invalid_login), I18n.t(:invalid_title), I18n.t(:invalid_content), I18n.t(:invalid_ip_address)])
      end
    end
  end
end
