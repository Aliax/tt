# frozen_string_literal: true

require 'rails_helper'

RSpec.describe PostsController, type: :controller do
  describe 'POST create' do
    let(:params) { { format: :json, title: 'Note', content: 'I know you dreams', login: 'Kushner', ip: '172.16.10.1/24' } }

    context 'when get valid parameters' do
      it 'returns status 200 and message for successful response' do
        post :create, params: params

        parsed_response = JSON.parse(response.body)
        expect(response).to have_http_status :ok
        expect(parsed_response['response']).to eq(I18n.t(:post_created_successfully))
      end
    end

    context 'when get invalid parameters' do
      it 'returns status 422 and error for invalid title' do
        params[:title] = nil
        post :create, params: params

        parsed_response = JSON.parse(response.body)
        expect(response).to have_http_status :unprocessable_entity
        expect(parsed_response['errors']).to eq([I18n.t(:invalid_title)])
      end

      it 'returns status 422 and error for invalid content' do
        params[:content] = nil
        post :create, params: params

        parsed_response = JSON.parse(response.body)
        expect(response).to have_http_status :unprocessable_entity
        expect(parsed_response['errors']).to eq([I18n.t(:invalid_content)])
      end

      it 'returns status 422 and error for invalid ip address' do
        params[:ip] = nil
        post :create, params: params

        parsed_response = JSON.parse(response.body)
        expect(response).to have_http_status :unprocessable_entity
        expect(parsed_response['errors']).to eq([I18n.t(:invalid_ip_address)])
      end
    end
  end

  describe 'POST update' do
    let!(:user) { User.create(login: 'Alf') }
    let!(:post) { user.posts.create(title: 'Alf\'s life', content: 'Frendly, eat ants', ip: '172.16.10.2/24') }
    let!(:rate) { Rate.create(post_id: post.id, value: 4) }
    let(:params) { { format: :json, id: post.id, rate_value: 2 } }

    context 'when get valid parameters' do
      it 'should respond with new post rating' do
        # binding.pry
        put :update, params: params

        parsed_response = JSON.parse(response.body)
        expect(response).to have_http_status :ok
        expect(parsed_response['response']).to eq(3)
      end
    end

    context 'when get valid parameters' do
      it 'should respond with 422 and error for empty id' do
        params[:id] = ''
        put :update, params: params

        parsed_response = JSON.parse(response.body)
        expect(response).to have_http_status :unprocessable_entity
        expect(parsed_response['errors']).to eq([I18n.t(:empty_id)])
      end

      it 'should respond with 422 and error for invalid rate value' do
        params[:rate_value] = nil
        put :update, params: params

        parsed_response = JSON.parse(response.body)
        expect(response).to have_http_status :unprocessable_entity
        expect(parsed_response['errors']).to eq([I18n.t(:invalid_rate_value)])
      end

      it 'should respond with 422 and few error' do
        params[:rate_value] = 6
        params[:id] = ''
        put :update, params: params

        parsed_response = JSON.parse(response.body)
        expect(response).to have_http_status :unprocessable_entity
        expect(parsed_response['errors']).to eq([I18n.t(:empty_id), I18n.t(:invalid_rate_value)])
      end
    end
  end

  describe 'GET index' do
    before do
      (0..11).each do |n|
        user = User.create(login: "user_#{n}")
        Post.create(title: "tit1_#{n}", content: "cont_#{n}", ip: '0.0.0.0', user_id: user.id, rate: 1).tap do |post|
          Rate.create(value: 2, post_id: post.id)
        end
        Post.create(title: "tit3_#{n}", content: "cont_#{n}", ip: '0.0.0.0', user_id: user.id, rate: 3).tap do |post|
          Rate.create(value: 2, post_id: post.id)
          Rate.create(value: 3, post_id: post.id)
          Rate.create(value: 4, post_id: post.id)
        end
        Post.create(title: "tit5_#{n}", content: "cont_#{n}", ip: '0.0.0.0', user_id: user.id, rate: 5).tap do |post|
          Rate.create(value: 5, post_id: post.id)
        end
      end
    end

    let(:params) { { format: :json, count_posts: 7 } }

    context 'when received count_posts' do
      it 'should return 11 posts title and content with avg rating' do
        get :index, params: params

        parsed_response = JSON.parse(response.body)
        expect(parsed_response['response']).to include(['tit3_0', 'cont_0'])
        expect(parsed_response['response'].count).to eq(7)
      end
    end

    context 'when is not received params' do
      it 'should return 10 posts title and content with avg rating' do
        params[:count_posts] = nil
        get :index, params: params

        parsed_response = JSON.parse(response.body)
        expect(parsed_response['response']).to include(['tit3_0', 'cont_0'])
        expect(parsed_response['response'].count).to eq(10)
      end
    end
  end

  describe 'GET ip_addresses' do
    context 'when notice non exist' do
      it 'should return empty array' do
        get :ip_addresses, params: { format: :json }

        parsed_response = JSON.parse(response.body)
        expect(parsed_response['response']).to eq([])
      end
    end

    context 'when noticies exists' do
      before do
        (0..5).each do |n|
          user = User.create(login: "user_#{n}")
          Post.create(title: "tit1_#{n}", content: "cont_#{n}", ip: '0.0.0.0', user_id: user.id, rate: 1)
          Post.create(title: "tit1_#{n}", content: "cont_#{n}", ip: '0.0.0.1', user_id: user.id, rate: 1)
        end
      end

      it 'should return array with data' do
        get :ip_addresses, params: { format: :json }

        parsed_response = JSON.parse(response.body)
        expect(parsed_response['response']).to eq([['0.0.0.0', '{user_0,user_1,user_2,user_3,user_4,user_5}'],
                                                   ['0.0.0.1', '{user_0,user_1,user_2,user_3,user_4,user_5}']])
      end
    end
  end
end
