# frozen_string_literal: true

class PostsController < ApiController
  def create
    result = CreatePostService.new(create_params).call

    if result.error_messages.empty?
      render_ok(message: I18n.t(:post_created_successfully))
    else
      render_unprocess_entity(message: result.error_messages)
    end
  end

  def update
    result = RatePostService.new(update_params).call

    if result.error_messages.empty?
      render_ok(message: result.post_rate)
    else
      render_unprocess_entity(message: result.error_messages)
    end
  end

  def index
    result = AverageRatingPostsService.new(index_params).call
    render_ok(message: result.posts)
  end

  def ip_addresses
    result = ActiveRecord::Base.connection.execute('select ip, array_agg(distinct (login)) from posts p join users u on (p.user_id = u.id) group by ip').values
    render_ok(message: result)
  end

  private

  def create_params
    params.permit(%i[title content login ip])
  end

  def update_params
    params.permit(%i[id rate_value])
  end

  def index_params
    params.permit(:count_posts)
  end
end
