class ApiController < ActionController::API
  rescue_from StandardError, with: :render_internal_server_error

  def render_ok(message: message)
    render status: :ok, json: { response: message }
  end

  def render_unprocess_entity(message: message)
    render status: :unprocessable_entity, json: { errors: message }
  end

  private

  def render_internal_server_error
    render status: :internal_server_error, json: { errors: I18n.t(:internal_server_error) }
  end
end