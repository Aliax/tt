# frozen_string_literal: true

class RatePostService
  VALID_RATE_VALUES = (1..5).to_a.freeze

  attr_reader :error_messages, :post_rate

  def initialize(params)
    @id = params[:id]
    @rate_value = params[:rate_value].to_i
    @error_messages = []
  end

  def call
    check_input_parameters
    calculate_and_add_rate if @error_messages.empty?
    self
  end

  private

  def check_input_parameters
    find_post
    check_rate_value
  end

  def find_post
    validate(error: :empty_id) { @id.present? }
    @user = Post.find_by(id: @id) if @id
  end

  def check_rate_value
    validate(error: :invalid_rate_value) { @rate_value && @rate_value.in?(VALID_RATE_VALUES) }
  end

  def calculate_and_add_rate
    sum_rates, count_rates = Rails.cache.fetch("post_#{@id}", expires_in: 0) do
      values = Rate.where(post_id: @id).pluck(:value)
      [values.sum, values.length]
    end
    @post_rate = (sum_rates + @rate_value) / (count_rates + 1).round
    CreateRateWorker.perform_async(@id, @rate_value)
  end

  def validate(error:)
    yield.tap { |validation_success| @error_messages << I18n.t(error) unless validation_success }
  end
end
