# frozen_string_literal: true

class CreatePostService
  attr_reader :error_messages

  def initialize(params)
    @title = params[:title]
    @content = params[:content]
    @login = params[:login]
    @ip_address = params[:ip]
    @error_messages = []
  end

  def call
    check_input_parameters
    create_post if error_messages.empty?
    self
  end

  private

  def check_input_parameters
    find_or_create_user
    title_valid?
    content_valid?
    ip_address_valid?
  end

  def find_or_create_user
    validate(error: :invalid_login) { @login.present? }
    @user = User.find_or_create_by!(login: @login) if @login
  end

  def title_valid?
    validate(error: :invalid_title) { @title.present? }
  end

  def content_valid?
    validate(error: :invalid_content) { @content.present? }
  end

  def ip_address_valid?
    validate(error: :invalid_ip_address) { @ip_address.present? && IPAddress(@ip_address) }
  end

  def create_post
    @user.posts.create!(title: @title, content: @content, ip: @ip_address)
  end

  def validate(error:)
    yield.tap { |validation_success| @error_messages << I18n.t(error) unless validation_success }
  end
end
