# frozen_string_literal: true

class AverageRatingPostsService
  DEFAULT_COUNT_POSTS = 10

  attr_reader :posts

  def initialize(params)
    @count_posts = params[:count_posts].to_i.positive? ? params[:count_posts].to_i : DEFAULT_COUNT_POSTS
  end

  def call
    @posts = ActiveRecord::Base.connection.execute("select title, content from posts where rate = (select round(avg(value)) from rates) limit #{@count_posts}").values
    self
  end
end
