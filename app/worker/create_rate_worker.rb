# frozen_string_literal: true
class CreateRateWorker
  include Sidekiq::Worker
  sidekiq_options queue: :low, retry: false, backtrace: true
  
  def perform(id, rate_value)
    add_new_rate(id, rate_value) && update_cache_post_id(id, rate_value) && calculate_avg_rate_for_post(id)
  end

  private

  def add_new_rate(id, rate_value)
    Rate.create!(post_id: id, value: rate_value)
  end

  def update_cache_post_id(id, rate_value)
    sum_rates, count_rates = Rails.cache.fetch("post_#{id}") { [0, 0] }
    Rails.cache.write("post_#{id}", [sum_rates + rate_value, count_rates + 1])
  end

  def calculate_avg_rate_for_post(id)
    sum_rates, count_rates = Rails.cache.fetch("post_#{id}") { [0, 0] }
    Post.find_by(id: id).update(rate: (sum_rates/count_rates).round)
  end
end