Rails.application.routes.draw do
  resources :posts, only: [:create, :update, :index] do
    collection do
      get :ip_addresses
    end
  end
end
